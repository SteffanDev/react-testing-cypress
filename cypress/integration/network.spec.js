/**
 * @author : Steffan Devotta
 * @date : March 12, 2020
 * @version : 1.0
 * @copyright : © 2010-2020 Information International Limited. All Rights Reserved
 */

describe('Network Requests', () => {

    //used to avoid unnecessary repetition
    beforeEach(() => {
        cy.visit('/')
    })

    it('displays random users from API', () => {
        cy.request('https://jsonplaceholder.typicode.com/users')
            .should((response) => {
                expect(response.status).to.eq(200)
                expect(response.body).to.have.length(10)
                expect(response).to.have.property('headers')
                expect(response).to.have.property('duration')
            })
    })



})