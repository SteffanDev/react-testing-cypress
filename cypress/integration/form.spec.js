/**
 * @author : Steffan Devotta
 * @date : March 12, 2020
 * @version : 1.0
 * @copyright : © 2010-2020 Information International Limited. All Rights Reserved
 */

describe('Form', () => {

    //used to avoid unnecessary repetition
    beforeEach(() => {
        cy.visit('/')
    })

    it('it focuses the input', () => {

        //the focussed DOM element is returned
        cy.focused().should('have.class', 'form-control')
    })

    it('accepts input', () => {

        //assign a variable
        const input = "Learn about Cypress"

        //cy.get returns the DOM element. Alternate cy.get('input')
        cy.get('.form-control')
            //cy.type is used to enter assigned previously
            .type(input)
            .should('have.value', input)
    })

    //checking the 'todo' drop down to be populated 
    it('displays list of todo', () => {
        cy.get('li')
            .should('have.length', 2)
    })

    //typing todo and adding it,then test for the length
    it('adds a new todo', () => {
        const input = "Learn about cypress"
        cy.get('.form-control')
            .type(input)
            .type('{enter}')
            .get('li')
            .should('have.length', 3)
    })

    //deletes a todo
    it('deletes a todo', () => {
        cy.get('li')
            .first()
            .find('.btn-danger')
            .click()
            .get('li')
            .should('have.length', 1)
    })

    //deletes all to do
    it('deletes all todo', () => {
        cy.get('li')
            .first()
            .find('.btn-danger')
            .click()
            .get('li')
            .first()
            .find('.btn-danger')
            .click()
            .get('.no-task')
            .should('have.text', 'All of your tasks are complete. Nicely done!')
    })
})