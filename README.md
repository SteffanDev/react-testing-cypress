This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### Cypress.io
This project has been integrated with cypress.io which is a new standard in front-end testing written from scratch , not based on Selenium. Cypress.io provides a complete end-to-end testing experience with the taste of automating .

## Available Scripts

In the project directory, you can run:


### `npm install cypress`
To install cypress
npm install cypress

### `npm run cypress`
Add  "cypress": "cypress open" to the scripts.
npm run cypress

### `npm start`
To start the react app

### To record test run
npx cypress run --record --key 6c1e2bde-fb7e-423f-bbd5-378106a21194 --spec "cypress/integration/form.spec.js"



Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.


